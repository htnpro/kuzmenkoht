var gulp         = require('gulp'),
    autoprefixer = require('gulp-autoprefixer'),
    sass         = require('gulp-sass'),
    browserSync  = require('browser-sync');

gulp.task('sass',  () => {
  return gulp.src('app/sass/**/*.sass')
    .pipe(sass({outputStyle: 'expanded'}))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
      }))
    .pipe(gulp.dest('app/css'))
    .pipe(browserSync.reload({stream: true}))
});

gulp.task('watch', ['browserSync', 'sass'], () => {
  gulp.watch('app/sass/**/*.sass', ['sass']);
  gulp.watch('app/*.html', browserSync.reload);
  gulp.watch('app/js/*.js', browserSync.reload);
});

gulp.task('browserSync', () => {
  browserSync({
    server: {
      baseDir: 'app/'
    },
    notify: false
  });
});